# sls-medium-feed

A serverless Medium (medium.com) post feed proxy.

Read more about it @ https://medium.com/@k1d_bl4ck/using-the-serverless-framework-to-add-your-medium-feed-to-a-website-903f8f32cf08
