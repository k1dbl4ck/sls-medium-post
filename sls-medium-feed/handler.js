const request = require('request');
const FeedMe = require('feedme');

module.exports.fetch = (event, context, callback) => {

  const username = 'k1d_bl4ck'; //replace with your Medium username
  const url = "https://medium.com/feed/@" + username;

  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
  }

  request({
    url: url,
    json: true
  }, (error, response, body) => {
    if (!error && response.statusCode === 200) {
      var parser = new FeedMe(true);
      parser.write(body.replace('])}while(1);</x>', ''));
      callback(null, {
        statusCode: 200,
        headers: headers,
        body: JSON.stringify(parser.done())
      });
    } else {
      callback(null, {
        statusCode: 500,
        headers: headers,
        body: error
      });
    }
  });
};